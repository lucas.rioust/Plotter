﻿using UnityEngine;
using System.Collections.Generic;
public class Place : Element
{
    public Vector2 Position;
    public List<Place> SubPlaces;
}
