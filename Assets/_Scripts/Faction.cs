﻿using System.Collections.Generic;

public class Faction : Element
{
    public Agenda Agenda;
    public List<Faction> SubFactions;
    public Alignement alignement;
    public Place QG;
    public List<Place> Auxiliaries;
}
