﻿using System.Collections.Generic;

public class Agenda : Element
{
    public List <Objective> objectives;
}

public enum ObjectiveStatus{Default, Success, Failure}
public class Objective : Element
{
    public ObjectiveStatus CurrentStatus;
    public List<Element> LinkedElements;
}
